﻿///
/// Did you ever wish that one field you needed was public instead of private?
/// I made this script just for those cases. 
/// After Building this project, add the exe and the mono.cecil file to your '<modname>Resource' folder
/// LLBMM will automaticly run the exe and make the changes for the end user.
/// 
/// The exe has to be named ASMRewriter.exe to automaticly run wil LLBMM
///

using System;
using System.IO;
using Mono.Cecil;
using Mono.Cecil.Inject;

namespace ASMRewriter
{
    class Program
    {
        private static readonly string ASMName = "Assembly-CSharp.dll";
        private static readonly string ModName = "AnimeModeMod";

        static void Main(string[] args)
        {
            if (args.Length > 0)
            {
                var finalASMPath = args[0].Replace("%20", " ");
                Directory.SetCurrentDirectory(finalASMPath);
            }

            Console.WriteLine("Reading assembly...");
            // Here Path.Combine is required for ASMR to run under linux
            //AssemblyDefinition modDef = AssemblyDefinition.ReadAssembly(
            //    Path.Combine(Directory.GetCurrentDirectory(), ModName + ".dll"),
            //    new ReaderParameters { ReadWrite = true }
            //);
            try
            {
                // Same thing here
                using (AssemblyDefinition aDef = AssemblyDefinition.ReadAssembly(
                    Path.Combine(Directory.GetCurrentDirectory(), ASMName),
                    new ReaderParameters { ReadWrite = true })
                )
                {
                    Console.WriteLine("--Setting methods public");
                    try
                    {
                        asm.SetMethodType(aDef, "LLHandlers.EffectHandler", "GetSpareEffect", ChangeType.Public);
                        asm.SetMethodType(aDef, "GameplayEntities.VisualEntity", "SetVisualSprite", ChangeType.Public);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                    }

                    try { aDef.Write(); } catch (Exception ex) { Console.WriteLine(ex); }
                    aDef.Dispose();
                }
            }
            catch
            {
                Console.WriteLine("Could not open assembly, press Enter to exit...");
            }

           //modDef.Dispose();
        }
    }

    public enum ChangeType
    {
        Public = 0,
        Virtual = 1,
        Abstract = 2
    }
}
