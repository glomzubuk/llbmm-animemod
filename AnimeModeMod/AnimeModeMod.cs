﻿using System;
using UnityEngine;
using LLHandlers;
using GameplayEntities;

namespace AnimeModeMod
{
    public class AnimeModeMod : MonoBehaviour
    {
        public static AnimeModeMod instance = null;
        public JOFJHDJHJGI currentGameState; // GameState currentGameState;
        public GameMode currentGameMode;

        public static void Initialize()
        {
            GameObject gameObject = new GameObject("AnimeModeMod");
            instance = gameObject.AddComponent<AnimeModeMod>();
            DontDestroyOnLoad(gameObject);
        }

        public ModMenuIntegration MMI = null;
        private KeyCode toggleConsoleKey = KeyCode.BackQuote;
        private KeyCode enableAuraKey = KeyCode.B;
        private KeyCode enablePhantomKey = KeyCode.N;
        private KeyCode enableDebugKey = KeyCode.K;
        private bool enableDebug = true;
        //private int auraSlider = -1;
        private int auraSpeedTrigger = 256;
        private int phantomSpeedTrigger = 128;
        private int blazeEffectHeight = 28;
        //private int silhouetteSlider = -1;
        //private bool enableLowHitlagOffline = false;
        private bool enableAuraOffline = true;
        private bool enableAuraOnline = false;
        //private bool lowHitlag = false;

        public GameplayEntities.BallEntity currentBall = null;
        bool inMatch = false;
        bool playersInAnime = false;
        bool playersInPhantom = false;

        private void Update()
        {
            if (MMI == null) { MMI = gameObject.AddComponent<ModMenuIntegration>(); }
            else
            {
                toggleConsoleKey = MMI.GetKeyCode("toggleConsole");
                enableAuraKey = MMI.GetKeyCode("enableAuraKey");
                enableDebugKey = MMI.GetKeyCode("enableDebugKey");
                //auraSlider = MMI.GetSliderValue("(slider)auraSlider");
                auraSpeedTrigger = MMI.GetInt("auraSpeedTrigger");
                phantomSpeedTrigger = MMI.GetInt("phantomSpeedTrigger");
                blazeEffectHeight = MMI.GetInt("blazeEffectHeight");
                //silhouetteSlider = MMI.GetSliderValue("(slider)silhouetteSlider");
                //enableLowHitlagOffline = MMI.GetTrueFalse("(bool)enableLowHitlag");
                enableAuraOffline = MMI.GetTrueFalse("enableAuraOffline");
                enableAuraOnline = MMI.GetTrueFalse("enableAuraOnline");
            }

            if (Input.GetKeyDown(enableDebugKey))
            {
                enableDebug = !enableDebug;
            }

            if (Input.GetKeyDown(toggleConsoleKey))
                LLScreen.UIScreen.StartShake(2, 200);

            currentGameState = DNPFJHMAIBP.HHMOGKIMBNM(); //GameStates.GetCurrent()
            currentGameMode = JOMBNFKIHIC.GIGAKBJGFDI.PNJOKAICMNN; //GameSettings.current.gameMode

            inMatch = (currentGameMode == GameMode._1v1 || currentGameMode == GameMode.TRAINING || currentGameMode == GameMode.FREE_FOR_ALL)
                && (currentGameState == JOFJHDJHJGI.CDOFDJMLGLO || currentGameState == JOFJHDJHJGI.LGILIJKMKOD);

            if (inMatch)
            {

                if (Input.GetKeyDown(enableAuraKey))
                {
                    CallOnPlayers(TurnPlayerIntoAnime);
                    playersInAnime = true;
                }
                if (Input.GetKeyDown(enablePhantomKey))
                {
                    CallOnPlayers(TurnPlayerIntoPhantom);
                    playersInPhantom = true;
                }

                if (playersInPhantom)
                {
                    CallOnPlayers(UpdatePlayerPhantom);
                }
                if (playersInAnime)
                {
                    CallOnPlayers(UpdatePlayerAnime);
                }
                if (LLHandlers.BallHandler.instance != null)
                    currentBall = LLHandlers.BallHandler.instance.GetBall();
                if (currentBall != null)
                {

                    /*if (Input.GetKey(KeyCode.N))
                    {
                        ball.SetVisualSprite("main2D", Texture2D.whiteTexture, true, false, BallEntity.GetFrameSize(), JKMAAHELEMF.DBOMOJGKIFI, 0f, false, 1f, Layer.GAMEPLAY, default(Color32));
                    }*/

                    bool isOnline = JOMBNFKIHIC.GDNFJCCCKDM; //  bool isOnline = GameSettings.isOnline;
                    if ((isOnline && enableAuraOnline) || (!isOnline && enableAuraOffline))
                    {

                        if (!playersInAnime && HHBCPNCDNDH.OCDKNPDIPOB(currentBall.GetPixelFlySpeed(true), new HHBCPNCDNDH(auraSpeedTrigger))) // floatf.>=(ball.GetFlySpeed(true), new floatf(250))
                        {
                            CallOnPlayers(TurnPlayerIntoAnime);
                            playersInPhantom = false;
                            playersInAnime = true;
                        } else if (!playersInPhantom && !playersInAnime && HHBCPNCDNDH.OCDKNPDIPOB(currentBall.GetPixelFlySpeed(true), new HHBCPNCDNDH(phantomSpeedTrigger))) // floatf.>=(ball.GetFlySpeed(true), new floatf(250))
                        {
                            CallOnPlayers(TurnPlayerIntoPhantom);
                            playersInPhantom = true;
                        }

                        if (currentBall.ballData.ballState == GameplayEntities.BallState.APPEAR || currentBall.ballData.ballState == GameplayEntities.BallState.APPEAR_HIGH)
                        {
                            CallOnPlayers(CancelPlayerAnimations);
                            playersInAnime = false;
                            playersInPhantom = false;
                        }
                    }
                }

            }
            else
            {
                currentBall = null;
                playersInAnime = false;
            }
        }


        private void CallOnPlayers(Action<PlayerEntity> callback)
        {
            ALDOKEMAOMB.ICOCPAFKCCE(delegate (GameplayEntities.PlayerEntity playerEntity) // Player.ForAllInMatch(delegate(Player player)
            {
                if (playerEntity.IsActivelyInMatch())
                {
                    callback(playerEntity);
                }
            });
        }

        private void TurnPlayerIntoPhantom(GameplayEntities.PlayerEntity playerEntity)
        {
            playerEntity.PlayAnim("phantom", "phantomVisual");
        }

        private void TurnPlayerIntoAnime(GameplayEntities.PlayerEntity playerEntity)
        {
            playerEntity.ChangeIntoSilhouette(true);
            playerEntity.PlayAnim("off", "phantomVisual");
            playerEntity.PlayAnim("aura", "auraVisual");
            CreateGetUpBlazeEffectOnPos(playerEntity.GetPosition(), playerEntity.GetTeam());
        }

        private void UpdatePlayerPhantom(GameplayEntities.PlayerEntity playerEntity)
        {
            playerEntity.PlayAnim("phantom", "phantomVisual");
        }

        private void UpdatePlayerAnime(GameplayEntities.PlayerEntity playerEntity)
        {
            playerEntity.ChangeIntoSilhouette(true);
            playerEntity.PlayAnim("aura", "auraVisual");
        }

        private void CancelPlayerAnimations(GameplayEntities.PlayerEntity playerEntity)
        {
            playerEntity.ChangeIntoSilhouette(false);
            playerEntity.PlayAnim("off", "phantomVisual");
            playerEntity.PlayAnim("off", "auraVisual");
        }

        public void CreateGetUpBlazeEffectOnPos(IBGCBLLKIHA pos, BGHNEHPFHGC team)
        {
            if (EffectHandler.noEffects)
            {
                return;
            }
            pos.CGJJEHPPOAN = HHBCPNCDNDH.GAFCIOAEGKD(pos.CGJJEHPPOAN, HHBCPNCDNDH.AJOCFFLIIIH(HHBCPNCDNDH.NKKIFJJEPOL(blazeEffectHeight), World.FPIXEL_SIZE));
            EffectEntity spareEffect = EffectHandler.instance.GetSpareEffect();
            EffectData effectData = new EffectData(1);
            effectData.active = true;
            effectData.SetPositionData(pos);
            effectData.offsetPixels = new JKMAAHELEMF(0, -20);
            effectData.frameSizePixels = new JKMAAHELEMF(400, 250);
            effectData.animSpeed = HHBCPNCDNDH.NKKIFJJEPOL(24.0m);
            effectData.scale = 1.2f;
            effectData.endAtAnimEnd = true;
            effectData.layer = Layer.INFRONT_GAMEPLAY;
            effectData.graphicName = "getDrawIn" + JPLELOFJOOH.OODCFCKNLOD(team);
            spareEffect.ApplyEffectData(effectData, false);
        }

        private void OnGUI()
        {
            GUI.contentColor = Color.red;
            if (enableDebug)
            {
                GUI.Label(new Rect(10, 10, 200, 25), "Debug Info: On");

                GUI.Label(new Rect(10, 35, 200, 25), "Is the game Online : " + (JOMBNFKIHIC.GDNFJCCCKDM ? "Yes" : "No"));
                GUI.Label(new Rect(10, 60, 500, 25), "The Big Question: " + (inMatch ? "Yes" : "No"));
                GUI.Label(new Rect(210, 35, 200, 25), "What's the debug key : " + enableDebugKey.ToString());
                GUI.Label(new Rect(210, 60, 200, 25), "What's the aura key : " + enableAuraKey.ToString());

                GUI.Label(new Rect(10, 85, 500, 25), "Current Game mode: " + currentGameMode.ToString());
                GUI.Label(new Rect(10, 110, 500, 25), "Current Game State: " + currentGameState.ToString());
                if (inMatch && currentBall != null)
                {
                    GUI.Label(new Rect(10, 135, 250, 25), "Ball State: " + currentBall.ballData.ballState.ToString());
                    GUI.Label(new Rect(260, 135, 500, 25), "Ball HitstunState: " + currentBall.ballData.hitstunState.ToString());
                    GUI.Label(new Rect(10, 160, 500, 25), "Ball FlySpeed: " + currentBall.GetFlySpeed(true));
                    GUI.Label(new Rect(10, 185, 500, 25), "Ball PixelFlySpeed: " + currentBall.GetPixelFlySpeed(true));
                }
            }
        }
    }
}


//GameplayEntities.AnimatableEntity;

//  DNPFJHMAIBP = GameStates

//public static GameState GetCurrent()
//=
//public static JOFJHDJHJGI HHMOGKIMBNM()

//public static Player Get(int playerNr)
//=
//public static ALDOKEMAOMB BJDPHEHJJJK(int BKEOPDPFFPM)
