// Script used to connect to ModMenu
using LLModMenu;

namespace AnimeModeMod
{
    public class ModMenuIntegration : ModMenuIntegrationBase
    {

        override protected void InitConfig()
        {
            WriteQueue writeQueue = new WriteQueue();
            /*
             * Mod menu now uses a single function to add options etc. (AddToWriteQueue)
             * your specified options should be added to this function in the same format as stated under
             * 
            Keybindings:
            AddToWriteQueue("(key)keyName", "LeftShift");                                       value can be: Any KeyCode as a string e.g. "LeftShift"

            Options:
            AddToWriteQueue("(bool)boolName", "true");                                          value can be: ["true" | "false"]
            AddToWriteQueue("(int)intName", "27313");                                           value can be: any number as a string. For instance "123334"
            AddToWriteQueue("(slider)sliderName", "50|0|100");                                  value must be: "Default value|Min Value|MaxValue"
            AddToWriteQueue("(header)headerName", "Header Text");                               value can be: Any string
            AddToWriteQueue("(gap)gapName", "identifier");                                      value does not matter, just make name and value unique from other gaps

            ModInformation:
            AddToWriteQueue("(text)text1", "Descriptive text");                                  value can be: Any string
            */


            // Insert your options here \/
            /*
            AddToWriteQueue("(key)enableAuraKey", "B");
            AddToWriteQueue("(key)enablePhantomKey", "N");
            AddToWriteQueue("(key)enableDebugKey", "K");
            AddToWriteQueue("(bool)enableLowHitlag", "false");
            AddToWriteQueue("(bool)enableAuraOffline", "true");
            AddToWriteQueue("(bool)enableAuraOnline", "false");
            AddToWriteQueue("(slider)silhouetteSlider", "250|0|1000");
            AddToWriteQueue("(slider)auraSlider", "250|0|1000");
            AddToWriteQueue("(int)auraSpeedTrigger", "250");
            AddToWriteQueue("(int)phantomSpeedTrigger", "100");
            AddToWriteQueue("(int)blazeEffectHeight", "28");
            */


            writeQueue.AddEntry("toggleConsole", "BackQuote", EntryType.KEY);
            writeQueue.AddEntry("enableAuraKey", "B", EntryType.KEY);
            writeQueue.AddEntry("enablePhantomKey", "N", EntryType.KEY);
            writeQueue.AddEntry("enableDebugKey", "K", EntryType.KEY);
            writeQueue.AddEntry("enableLowHitlag", "false", EntryType.BOOLEAN);
            writeQueue.AddEntry("enableAuraOffline", "true", EntryType.BOOLEAN);
            writeQueue.AddEntry("enableAuraOnline", "false", EntryType.BOOLEAN);
            writeQueue.AddEntry("silhouetteSlider", "250|0|1000", EntryType.SLIDER);
            writeQueue.AddEntry("auraSlider", "250|0|1000", EntryType.SLIDER);
            writeQueue.AddEntry("auraSpeedTrigger", "250", EntryType.NUMERIC);
            writeQueue.AddEntry("phantomSpeedTrigger", "100", EntryType.NUMERIC);
            writeQueue.AddEntry("blazeEffectHeight", "28", EntryType.NUMERIC);


            this.config = ModMenu.Instance.configManager.InitConfig(gameObject.name, writeQueue);
            writeQueue.Clear();
        }
    }
}